package com.github.nyavro.conversion.bson

import com.nyavro.conversion.core.bean.BeanCachedConversion
import com.nyavro.conversion.core.document.DocumentCachedConversion
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}

import scala.reflect.ClassTag
import scala.reflect.runtime.universe._


object Transformation {

  implicit def bsonToBean[A:TypeTag]:BSONDocumentReader[A] = {
    object DocumentToBeanTransformation extends BSONDocumentReader[A] {
      val conversion = new DocumentCachedConversion(BSONToBeanFactory)
      override def read(doc: BSONDocument): A = conversion.convert[A](doc).get
    }
    DocumentToBeanTransformation
  }
  
  implicit def beanToBson[A:ClassTag]:BSONDocumentWriter[A] = {
    object BeanTransformation extends BSONDocumentWriter[A] {
      val conversion = new BeanCachedConversion(BeanToBSONFactory)
      override def write(bean: A): BSONDocument = conversion.convert(bean)
    }
    BeanTransformation
  }
}
