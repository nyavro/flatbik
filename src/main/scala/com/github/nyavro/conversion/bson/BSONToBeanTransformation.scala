package com.github.nyavro.conversion.bson

import com.nyavro.conversion.core.document.{DocumentConversion, DocumentTransformation, DocumentTransformationFactory}
import reactivemongo.bson._

import scala.reflect.runtime.universe._

/**
 * Defines transformation of BSONDocument components to scala class
 * @param conversion upper level DocumentConversion
 */
class BSONToBeanTransformation(conversion: DocumentConversion[BSONDocument]) extends DocumentTransformation[BSONDocument] {

  override def convert(typ: Type, value: Any) = value match {
      case Some(x) => x match {
        case BSONString(s) => Some(s)
        case BSONInteger(i) => Some(i)
        case BSONDouble(d) => Some(d)
        case id:BSONObjectID => Some(Some(id))
        case doc:BSONDocument => conversion.convert(typ)(doc)
      }
      case _ => None
    }

  override def property(holder: BSONDocument, name: String): Any = holder.get(name)
}

object BSONToBeanFactory extends DocumentTransformationFactory[BSONDocument] {
  override def transformation(conversion: DocumentConversion[BSONDocument]) = new BSONToBeanTransformation(conversion)
}
