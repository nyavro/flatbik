package com.github.nyavro.conversion.bson

import com.nyavro.conversion.core.bean.{BeanTransformationFactory, BeanTransformation, BeanConversion}
import reactivemongo.bson._
import reactivemongo.bson.BSONDouble
import reactivemongo.bson.BSONString
import reactivemongo.bson.BSONInteger

/**
 * Created by eny on 10/11/14.
 */
class BeanToBSONTransformation(conversion: BeanConversion[Any, BSONDocument]) extends BeanTransformation[Any, BSONDocument] {

  override def convert(bean: Any) = bean match {
    case d:Double => BSONDouble(d)
    case i:Int => BSONInteger(i)
    case s:String => BSONString(s)
    case o:Option[BSONObjectID] => o match {
        case Some(id) => id
        case None => BSONObjectID.generate
      }
    case v:Any => conversion.convert(v)
  }

  override def translate(key:String) =
    if(key.equals("id")) "_id"
    else key

  override def bind(components: Map[String, Any]): BSONDocument = BSONDocument(components.map{case (k,v)=> k -> v.asInstanceOf[BSONValue]})
}

object BeanToBSONFactory extends BeanTransformationFactory[Any, BSONDocument] {
  override def transformation(conversion: BeanConversion[Any, BSONDocument]) = new BeanToBSONTransformation(conversion)
}