package com.github.nyavro.conversion.bson

import org.scalatest.{Matchers, FunSpec}
import reactivemongo.bson._

/**
 * Created by eny on 10/12/14.
 */
private case class TestFlatBean2(a:String, b:Int)
private case class TestComplexBean2(c:String, d:Double, e:TestFlatBean2)
private case class TestBeanWithId2(id:Option[BSONObjectID], a:String)
class DocumentTransformationTest extends FunSpec with Matchers {
  describe("Converts document to bean") {
    it("Converts plain class") {
      val doc = BSONDocument(Map("a"->BSONString("val1"), "b"->BSONInteger(12)))
      Transformation.bsonToBean[TestFlatBean2].read(doc) should === (TestFlatBean2("val1", 12))
    }
    it("Converts complex class") {
      val doc = BSONDocument(Map("c"->BSONString("val3"), "d"->BSONDouble(11.5), "e"->BSONDocument(Map("a"->BSONString("val2"), "b"->BSONInteger(17)))))
      val read: TestComplexBean2 = Transformation.bsonToBean[TestComplexBean2].read(doc)
      read should === (TestComplexBean2("val3", 11.5, TestFlatBean2("val2", 17)))
    }
    it("Converts bean with id") {
      val id = BSONObjectID.generate
      val value = "aValue"
      val doc = BSONDocument(Map("id"->id, "a"->BSONString(value)))
      val read: TestBeanWithId2 = Transformation.bsonToBean[TestBeanWithId2].read(doc)
      read should === (TestBeanWithId2(Some(id), value))
    }
  }
}
