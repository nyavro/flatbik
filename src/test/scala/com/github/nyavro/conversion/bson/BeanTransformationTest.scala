package com.github.nyavro.conversion.bson

import org.scalatest.{FunSpec, Matchers}
import reactivemongo.bson.{BSONObjectID, BSONDocument}

/**
 * Created by eny on 9/30/14.
 */
private case class TestFlatBean(a:String, b:Int, c:Double)
private case class TestComplexBean(a:String, b:Int, c:TestFlatBean)
private case class TestBeanWithId(id:Option[BSONObjectID], a:String)

class BeanTransformationTest extends FunSpec with Matchers {
  describe("Converts bean to BSONDocument") {
    it("Converts flat bean") {
      val bean = TestFlatBean("valueA", 11, 0.5)
      val doc: BSONDocument = Transformation.beanToBson[TestFlatBean].write(bean)
      doc.getAs[String]("a") should === (Some("valueA"))
      doc.getAs[Int]("b") should === (Some(11))
      doc.getAs[Double]("c") should === (Some(0.5))
    }
    it("Converts complex bean") {
      val bean = TestComplexBean("valueB", 12, TestFlatBean("valueC", 13, 0.25))
      val doc: BSONDocument = Transformation.beanToBson[TestComplexBean].write(bean)
      doc.getAs[String]("a") should === (Some("valueB"))
      doc.getAs[Int]("b") should === (Some(12))
      for (
        nested <-doc.getAs[BSONDocument]("c");
        a <- nested.getAs[String]("a");
        b <- nested.getAs[Int]("b");
        c <- nested.getAs[Double]("c")
      ) yield (a,b,c) should === ("valueC", 13, 0.25)
    }
    it("Converts bean with id") {
      val id = BSONObjectID.generate
      val bean = TestBeanWithId(Some(id), "valueA")
      val doc = Transformation.beanToBson[TestBeanWithId].write(bean)
      doc.getAs[BSONObjectID]("_id") should === (Some(id))
    }
  }
}
